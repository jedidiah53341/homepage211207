# Homepage211207
Not a project, just a single page design that uses customized Bootstrap 5. Coded with mobile-first in mind. I tried to exploit several CSS animations and transitions within the limit of a no-javascript environment. Though javascript is virtually supported nowadays, some 'data saving' mode found in several mobile browsers usually refrain loading javascript. 

There are several shortcomings, though. The card's height needed to be expressed in pixels. Thus, too much text on those card could cause unwanted clipping. The jumbotron's background image is also hardcoded to the CSS so it may be impractical to change those images flexibly. 

Monitor view : https://youtu.be/fFgnK1bTH7c
Smartphone view : https://youtu.be/eU-k2306TnA
